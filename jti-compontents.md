# JTI COMPONENT LIST

## Heading
- text
- h1,h2,h3,h4,h5 selection
- alignment: center, left, right

## Text
- text
- alignment: center, left, right

## Link
- Link url
- Link text
- style: button/inline (underline with arrow)

## Image
- Image

## Video
- Video
- Video title (optional)

## Blockquote
- Quote
- subheading (optional)
- alignment: left/right (determines placement of bar)
(different styling for blockquote within 2 column layout)

## Chart 
- Title
- x axis (labels)
- y axis (labels)

- Per option
    - Legend label
    - x/y values 

## 2 column layout 
- can drop in any of the above components 

___

## Employee
- Name
- Pulls following info: 
    - Photo
    - Position
    - Date started
    - Location
    - Reports to 

## Employee row
- 1-2 : include all fields
- 3: include name, photo, position, and read more link
- >4: include all fields except photo

## Employee celebration callout
- Pre title
- Title
- Employees (will center align)
    - pulls name, position


__

## Riley questions
- uneven widths of 2 column layout
- inconsistent text width (h2/p)
- spacing under button
- spacing between images

- spacing between image and text between column layouts